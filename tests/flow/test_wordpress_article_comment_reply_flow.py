from base64 import b64encode
import requests
import pytest
import lorem

username_1 = 'editor'
password_1 = 'HWZg hZIP jEfK XCDE V9WM PQ3t'
email_1 = 'editor@somesite.com'
username_2 = 'commenter'
password_2 = 'SXlx hpon SR7k issV W2in zdTb'
email_2 = 'commenter@somesite.com'
blog_url = 'https://gaworski.net'
posts_endpoint_url = blog_url + "/wp-json/wp/v2/posts"
comments_endpoint_url = blog_url + '/wp-json/wp/v2/comments'
token_1 = b64encode(f"{username_1}:{password_1}".encode('utf-8')).decode("ascii")
token_2 = b64encode(f"{username_2}:{password_2}".encode('utf-8')).decode("ascii")


@pytest.fixture(scope='module')
def article():
    owner_sign = 'Artur Drozd'
    article = {
        "article_title": "This is a joke created by " + str(owner_sign),
        "article_subtitle": lorem.sentence(),
        "article_text": lorem.paragraph(),
        "author": username_1
    }
    return article


@pytest.fixture(scope='module')
def editor_headers():
    e_headers = {
        "Content-Type": "application/json",
        "Authorization": "Basic " + token_1
    }
    return e_headers


@pytest.fixture(scope='module')
def commenter_headers():
    c_headers = {
        "Content-Type": "application/json",
        "Authorization": "Basic " + token_2
    }
    return c_headers


@pytest.fixture(scope='module')
def posted_article(article, editor_headers):
    payload = {
        "title": article["article_title"],
        "excerpt": article["article_subtitle"],
        "content": article["article_text"],
        "status": "publish",
        "post_author": article["author"]
    }
    print(posts_endpoint_url)
    response = requests.post(url=posts_endpoint_url, headers=editor_headers, json=payload)
    return response


def test_new_post_is_successfully_created(posted_article):
    assert posted_article.status_code == 201
    assert posted_article.reason == "Created"


def test_new_post_is_created_by_username1(posted_article):
    assert posted_article.json()['author'] == 2


def test_newly_created_post_can_be_read(article, posted_article):
    post_id = posted_article.json()['id']
    post_url = f'{posts_endpoint_url}/{post_id}'
    published_article = requests.get(url=post_url)
    assert published_article.status_code == 200
    assert published_article.reason == 'OK'


def test_new_comment_is_successfully_created_in_post(commenter_headers, posted_article):
    post_id = posted_article.json()['id']
    comment_content = "Jakie jest podobieństwo między wigwamem, a linuksem?"

    payload_commenter = {
        "post": post_id,
        "content": comment_content
    }

    response = requests.post(url=comments_endpoint_url, headers=commenter_headers, json=payload_commenter)
    assert response.status_code == 201
    assert response.reason == "Created"

    comment_id = response.json()['id']
    comment_url = f'{comments_endpoint_url}/{comment_id}'
    comment_response = requests.get(url=comment_url)

    assert comment_response.status_code == 200
    assert comment_response.reason == 'OK'
    assert comment_response.json()['post'] == post_id


def test_new_replay_is_successfully_created_for_comment(editor_headers, commenter_headers, posted_article):
    comment_content = "Jakie jest podobieństwo między wigwamem, a linuksem?"

    replay_content = "No windows, no gates, Apache inside"
    post_id = posted_article.json().get('id')

    payload_commenter = {
        "post": post_id,
        "content": comment_content
    }
    response = requests.post(url=comments_endpoint_url, headers=commenter_headers, json=payload_commenter)
    comment_id = response.json().get('id')

    comment_response = requests.get(url=f'{comments_endpoint_url}?post={post_id}')
    if comment_response.status_code == 200:
        comments = comment_response.json()
        if len(comments) > 0:
            comment_id = comments[0]['id']

    assert comment_id is not None

    post_url = f'{posts_endpoint_url}/{post_id}'
    post_response = requests.get(url=post_url)
    assert post_response.status_code == 200
    post_author = post_response.json()['author']

    payload_editor = {
        "post": post_id,
        "content": replay_content,
        "parent": comment_id,
        "author": post_author
    }

    editor_response = requests.post(url=comments_endpoint_url, headers=editor_headers, json=payload_editor)
    assert editor_response.status_code == 201
    assert editor_response.reason == "Created"

    reply_id = editor_response.json()['id']
    reply_url = f'{comments_endpoint_url}/{reply_id}'
    reply_response = requests.get(url=reply_url)

    assert reply_response.status_code == 200
    assert reply_response.reason == 'OK'
    assert reply_response.json()['post'] == post_id
    assert reply_response.json()['parent'] == comment_id
